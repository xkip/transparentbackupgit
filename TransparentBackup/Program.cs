﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace TransparentBackup
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			var svc = new TransparentBackupService();

			if (args.Any(x => x == "console"))
			{
				Console.WriteLine("Starting...");
				svc.PerformStart();
				Console.WriteLine("Started. Press enter to stop.");
				Console.ReadLine();
				Console.WriteLine("Stopping...");
				svc.PerformStop();
				Console.WriteLine("Stopped.");
			}
			else
			{
				ServiceBase.Run(new[] { svc });
			}
		}
	}
}
