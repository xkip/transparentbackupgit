using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Threading;

using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;

namespace TransparentBackup.Tests
{
	//	static class WaitHandleFromEventTest
	//	{
	//		public static event Action<int> testEvent1;
	//		public static event EventHandler testEvent2;
	//		public static event EventHandler<EventArgs> testEvent3;
	//		
	//	}

	public static class WaitEvent
	{

		//		static void test()
		//		{
		//			Get(x => WaitHandleFromEventTest.testEvent1 += delegate { x(); });
		//			Get(x => WaitHandleFromEventTest.testEvent2 += delegate { x(); });
		//			Get(x => WaitHandleFromEventTest.testEvent3 += delegate { x(); });
		//
		//			Wait(x => WaitHandleFromEventTest.testEvent3 += delegate { x(); });
		//		}

		public static AutoResetEvent Get(Action<Action> subscriber)
		{
			var eventWaitHandle = new AutoResetEvent(false);
			subscriber(() => eventWaitHandle.Set());
			return eventWaitHandle;
		}

		public static void Wait(Action<Action> subscriber)
		{
			Get(subscriber).WaitOne();
		}
	}

	public class BackupController : IDisposable
	{
		readonly ControllerConfiguration _controllerConfig;

		public BackupController(ControllerConfiguration controllerConfig)
		{
			if (controllerConfig == null)
			{
				throw new ArgumentNullException("controllerConfig");
			}
			_controllerConfig = controllerConfig;
		}

		public BackupController()
		{
			_controllerConfig = new ControllerConfiguration();
		}

		public BackupController(string sourcePath, string backupPath)
			: this()
		{
			if (sourcePath == null)
			{
				throw new ArgumentNullException("sourcePath");
			}
			if (backupPath == null)
			{
				throw new ArgumentNullException("backupPath");
			}
			SourcePath = sourcePath;
			BackupPath = backupPath;
		}

		public string SourcePath
		{
			get { return Config.From; }
			set
			{
				if (!Path.IsPathRooted(value))
				{
					throw new Exception("Path is not rooted");
				}
				Config.From = PreparePath(value);
			}
		}

		static string PreparePath(string value)
		{
			return value.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar).TrimEnd(Path.DirectorySeparatorChar);
		}

		public string BackupPath
		{
			get { return Config.To; }
			set
			{
				if (!Path.IsPathRooted(value))
				{
					throw new Exception("Path is not rooted");
				}
				Config.To = PreparePath(value);
			}
		}

		bool IsStarted
		{
			get { return _isStarted; }
		}

		public ManualResetEvent PrimarySyncDoneWaitHandle
		{
			get { return _primarySyncDoneEvent; }
		}

		public void Start()
		{
			lock (_startingSync)
			{
				if (_isDisposing)
				{
					throw new ObjectDisposedException(null);
				}
				if (IsStarted)
				{
					throw new InvalidOperationException("Already started");
				}

				while (!Directory.Exists(SourcePath))
				{
					// waiting for drive
					Thread.Sleep(1000);
				}

				// auto create for last level
				if (Directory.Exists(Path.GetDirectoryName(BackupPath)))
				{
					Directory.CreateDirectory(BackupPath);
				}

				if (!Directory.Exists(BackupPath))
				{
					throw new InvalidOperationException("BackupPath is not configured");
				}

				_thread = new Thread(Worker) {Name = "MyBackuper"};
				_thread.Start();

				_watcher = new FileSystemWatcher(SourcePath, "*.*")
				{
					IncludeSubdirectories = true,
					EnableRaisingEvents = true,
					InternalBufferSize = 4096 * 1024, // For best performance, use a multiple of 4 KB on Intel-based computers
					NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.Size,
				};
				_watcher.Renamed += WatcherRenamed;
				_watcher.Error += WatcherError;
				_watcher.Deleted += WatcherDeleted;
				_watcher.Created += WatcherCreated;
				_watcher.Changed += WatcherChanged;

				// http://msdn.microsoft.com/query/dev10.query?appId=Dev10IDEF1&l=EN-US&k=k(SYSTEM.IO.FILESYSTEMWATCHER.INTERNALBUFFERSIZE);k(TargetFrameworkMoniker-%22.NETFRAMEWORK%2cVERSION%3dV4.0%22);k(DevLang-CSHARP)&rd=true

				_isStarted = true;
			}
		}

		bool _isStarted;

		readonly Queue<string> _eventsQueue = new Queue<string>(64);
		readonly HashSet<string> _eventsQueueSet = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
		readonly AutoResetEvent _eventsEnqueued = new AutoResetEvent(false);

		void Enqueue(string path)
		{
			if (_doNotSyncBrockenFolders)
			{
				return;
			}

			lock (_eventsQueue)
			{
				if (_eventsQueueSet.Add(path))
				{
					_eventsQueue.Enqueue(path);
					_eventsEnqueued.Set();
				}
			}
		}

		string Dequeue()
		{
			lock (_eventsQueue)
			{
				if (_eventsQueue.Count > 0)
				{
					var path = _eventsQueue.Dequeue();
					_eventsQueueSet.Remove(path);
					return path;
				}
				return null;
			}
		}

		void WatcherChanged(object sender, FileSystemEventArgs e)
		{
			// XTrace.XTrace.Verbose("Watcher_Changed", e.FullPath);
			Enqueue(e.FullPath);
		}

		void WatcherCreated(object sender, FileSystemEventArgs e)
		{
			// XTrace.XTrace.Verbose("Watcher_Created", e.FullPath);
			Enqueue(e.FullPath);
		}

		void WatcherRenamed(object sender, RenamedEventArgs e)
		{
			// XTrace.XTrace.Verbose("Watcher_Renamed", e.FullPath);
			Enqueue(e.FullPath);
			Enqueue(Path.GetDirectoryName(e.OldFullPath));
		}

		void WatcherDeleted(object sender, FileSystemEventArgs e)
		{
			// XTrace.XTrace.Verbose("Watcher_Deleted", e.FullPath);
			Enqueue(Path.GetDirectoryName(e.FullPath));
		}

		/// <summary>
		/// e.g. Internal buffer owerflow
		/// </summary>
		void WatcherError(object sender, ErrorEventArgs e)
		{
			// XTrace.XTrace.Exception(e.GetException(), "WatcherError");
		}

		void ProcessEvent(string path)
		{
			if (File.Exists(path))
			{
				BackupFile(path);
			}
			else if (Directory.Exists(path))
			{
				BackupDirectory(path);
			}
			else
			{
				XTrace.XTrace.Verbose("ProcessEvent", "It is not file and not directory: " + path);
			}
		}

		void BackupFile(string fullPath)
		{
			var ctx = new Context
			{
				BaseFrom = SourcePath,
				BaseTo = BackupPath,
				RestoreMode = false,
			};
			if (AllowCopy(new FileInfo(fullPath)))
			{
				CopyFile(ref ctx, fullPath);
			}
		}

		void BackupDirectory(string fullPath)
		{
			var ctx = new Context
			{
				BaseFrom = SourcePath,
				BaseTo = BackupPath,
				RestoreMode = false,
			};
			if (AllowCopy(new DirectoryInfo(fullPath)))
			{
				CopyDirectory(ref ctx, fullPath);
			}
		}

		Thread _thread;
		FileSystemWatcher _watcher;
		bool _isDisposing;
		bool _isDisposingBackup; // backup phase of disposal
		readonly object _startingSync = new object();

		FileStream _lockerSource;
		FileStream _lockerBackup;

		TimeSpan _delay = TimeSpan.FromSeconds(30);

		public TimeSpan Delay
		{
			get { return _delay; }
			set { _delay = value; }
		}

		TimeSpan _delayForAll = TimeSpan.FromMinutes(5);

		public TimeSpan FullBackupEvery
		{
			get { return _delayForAll; }
			set
			{
				if (value <= TimeSpan.Zero)
				{
					// infinite for EventWaithandle.WaitOne(TimeStamp)
					value = TimeSpan.FromMilliseconds(-1);
				}
				_delayForAll = value;
			}
		}

		public ControllerConfiguration Config
		{
			get { return _controllerConfig; }
		}

		void Worker()
		{
			try
			{
				WorkerPrimarySync();

				// pooling
				while (!_isDisposing || _eventsQueue.Count > 0)
				{
					if (_eventsEnqueued.WaitOne(FullBackupEvery))
					{
						XTrace.XTrace.Verbose("Worker_Queued");
						_delayWaitEvent.WaitOne(Delay);
						XTrace.XTrace.Verbose("Worker_Delayed");

						CheckAndRestoreSource();

						while (true)
						{
							var path = Dequeue();
							if (path == null)
							{
								break;
							}
							try
							{
								ProcessEvent(path);
							}
							catch (Exception ex)
							{
								XTrace.XTrace.Exception(ex);
								// XTrace.XTrace.Information("Reenqueue", path);
								// Enqueue(path);
							}
							XTrace.XTrace.Verbose("Worker_Event_Processed", path);
						}
					}
					else if (!_isDisposing)
					{
						CheckAndRestoreSource();
						try
						{
							CopyDirectoryBackup();
						}
						catch (Exception ex)
						{
							XTrace.XTrace.Exception(ex);
						}
					}
				}
			}
			catch (Exception ex)
			{
				XTrace.XTrace.Exception(ex, "BackupController Worker");
			}
		}

		void CheckAndRestoreSource()
		{
			if (string.IsNullOrEmpty(MarkTryRead(SourcePath, true)))
			{
				// waiting for drive
				while (!Directory.Exists(SourcePath))
				{
					Thread.Sleep(1000);
				}
				WorkerPrimarySync();
			}
		}

		volatile bool _doNotSyncBrockenFolders;

		void WorkerPrimarySync()
		{
			try
			{
				// startup check
				//				var sourceId = Path.Combine(SourcePath, _controllerIdFile);
				//				var backupId = Path.Combine(BackupPath, _controllerIdFile);
				//				var sourceLocker = Path.Combine(SourcePath, _controllerLockerFile);
				//				var backupLocker = Path.Combine(BackupPath, _controllerLockerFile);

				var backId = MarkTryRead(BackupPath, false);

				if (!string.IsNullOrEmpty(backId))
				{
					var srcId = MarkTryRead(SourcePath, true);
					if (!string.IsNullOrEmpty(srcId))
					{
						// compare
						if (!String.Equals(backId, srcId, StringComparison.InvariantCultureIgnoreCase))
						{
							_doNotSyncBrockenFolders = true;
							throw new Exception("Controller id is different. Try to clear either backup or source");
						}
					}
					else
					{
						// ALARM, source is destroyed
						//File.Copy(backupId, sourceId);
						//CopyFile(BackupPath, SourcePath, backupId, true);
						CopyDirectoryRestore();
						MarkWrite2(SourcePath, backId);
					}
				}
				else
				{
					// no backups exists
					var id = NewControllerId();
					MarkWrite2(SourcePath, id);
					MarkWrite2(BackupPath, id);
				}

				try
				{
					CopyDirectoryBackup();
				}
				catch (Exception ex)
				{
					XTrace.XTrace.Exception(ex);
				}
			}
			catch
			{
				ThreadPool.QueueUserWorkItem(delegate { Dispose(); });
				throw;
			}
			finally
			{
				OnPrimarySyncDone();
			}
		}

		static void MarkWrite2(string folder, string id)
		{
			var markFile = Path.Combine(folder, _controllerIdFile2);
			File.WriteAllText(markFile, id);
			File.SetAttributes(markFile, File.GetAttributes(markFile) | FileAttributes.Hidden | FileAttributes.System);
		}

		static string MarkTryRead(string folder, bool isSourceOrTarget)
		{
			var markFile2 = Path.Combine(folder, _controllerIdFile2);
			if (File.Exists(markFile2))
			{
				return File.ReadAllText(markFile2);
			}
			var markFile = Path.Combine(folder, _controllerIdFile);
			if (File.Exists(markFile))
			{
				return File.ReadAllText(markFile);
			}
			if (Directory.Exists(Path.GetPathRoot(folder)))
			{
				// drive is ready... if this is RAM drive (source) or just new configuration (target) - we can just make folder and go ahed on next scan
				Directory.CreateDirectory(folder);
			}
			return null;
		}

		public event EventHandler PrimarySyncDone;

		protected void OnPrimarySyncDone()
		{
			_primarySyncDoneEvent.Set();
			var handler = PrimarySyncDone;
			if (handler != null)
			{
				handler(this, EventArgs.Empty);
			}
		}

		readonly ManualResetEvent _primarySyncDoneEvent = new ManualResetEvent(false);
		readonly AutoResetEvent _delayWaitEvent = new AutoResetEvent(false);

		const string _controllerIdFile = "_backupControlelrId.TransparentBackup";
		const string _controllerIdFile2 = "_backupControllerId.TransparentBackup";
		const string _controllerLockerFile = "_lock.TransparentBackup";

		string NewControllerId()
		{
			return Guid.NewGuid().ToString("N");
		}

		//		string GetTargetFileName(string file)
		//		{
		//			return Path.Combine(BackupPath, file);
		//		}

		void CopyDirectoryBackup()
		{
			var ctx = new Context
			{
				BaseFrom = SourcePath,
				BaseTo = BackupPath,
				RestoreMode = false,
			};
			CopyDirectory(ref ctx, SourcePath);
		}

		void CopyDirectoryRestore()
		{
			var ctx = new Context
			{
				BaseFrom = BackupPath,
				BaseTo = SourcePath,
				RestoreMode = true,
			};
			CopyDirectory(ref ctx, BackupPath);
		}

		void CopyDirectory(ref Context ctx, string fullPath)
		{
			CopyDirectory(ref ctx, new DirectoryInfo(fullPath));
		}

		void CopyDirectory(ref Context ctx, DirectoryInfo dir)
		{
			if (_doNotSyncBrockenFolders)
			{
				return;
			}
			var targetDir = ctx.GetPathInTarget(dir.FullName);
			var targetDirInfo = new DirectoryInfo(targetDir);

			Directory.CreateDirectory(targetDir);
			if (targetDirInfo.Attributes != dir.Attributes)
			{
				targetDirInfo.Attributes = dir.Attributes;
			}

			var sourceDirs = dir.GetDirectories();
			var sourceDirsMap = sourceDirs.ToDictionary(x => x.Name);
			var sourceDirsFiltered = sourceDirs.Where(AllowCopy);
			foreach (var dirInfo in targetDirInfo.GetDirectories().ToArray())
			{
				if (!sourceDirsMap.ContainsKey(dirInfo.Name))
				{
					IoSafe(() => dirInfo.Delete(true));
				}
			}

			foreach (var subdir in sourceDirsFiltered)
			{
				CopyDirectory(ref ctx, subdir);
			}

			var sourceFiles = dir.GetFiles();
			var sourceFilesMap = sourceFiles.ToDictionary(x => x.Name);
			var sourceFilesFiltered = sourceFiles.Where(AllowCopy); //.ToDictionary(x => x.Name);
			// propogate removal
			foreach (var fileInfo in targetDirInfo.GetFiles().ToArray())
			{
				if (!sourceFilesMap.ContainsKey(fileInfo.Name))
				{
					IoSafe(() =>
					{
						fileInfo.IsReadOnly = false;
						fileInfo.Delete();
					});
				}
			}
			// propogate changes
			foreach (var file in sourceFilesFiltered)
			{
				CopyFile(ref ctx, file);
			}
		}

		readonly Lazy<string> _tmp = new Lazy<string>(() =>
		{
			var tmp = Path.GetTempPath();
			return Path.IsPathRooted(tmp) ? tmp : Guid.NewGuid().ToString("m");
		});

		bool AllowCopy(FileSystemInfo info)
		{
			if ((info.FullName + Path.DirectorySeparatorChar).StartsWith(_tmp.Value, StringComparison.InvariantCultureIgnoreCase))
			{
				return false;
			}

			var excludeAttribs = FileAttributes.System | FileAttributes.Temporary;
			if (!Config.IncludeHidden)
			{
				excludeAttribs |= FileAttributes.Hidden;
			}
			return (info.Attributes & (excludeAttribs)) <= 0;
		}

		struct Context
		{
			public string BaseFrom;
			public string BaseTo;
			public bool RestoreMode;

			public string GetPathInTarget(string path)
			{
				return GetPathIn(path, BaseFrom, BaseTo);
			}

			public string GetPathInSource(string path)
			{
				return GetPathIn(path, BaseTo, BaseFrom);
			}

			public string GetPathInOtherBase(string path)
			{
				if (path.StartsWith(BaseFrom, StringComparison.OrdinalIgnoreCase))
				{
					return GetPathInTarget(path);
				}
				return GetPathInSource(path);
			}

			public string GetPathIn(string path, string basePath, string otherBase)
			{
#if DEBUG
				if (!path.StartsWith(basePath, StringComparison.OrdinalIgnoreCase))
				{
					throw new ArgumentException("File is not from base folder");
				}
#endif
				var sourceRelative = path.Substring(basePath.Length).TrimStart(Path.DirectorySeparatorChar);
				return Path.Combine(otherBase, sourceRelative);
			}
		}

		void CopyFile(ref Context ctx, string path)
		{
			CopyFile(ref ctx, new FileInfo(path));
		}

		void CopyFile(ref Context ctx, FileInfo file)
		{
			if (_doNotSyncBrockenFolders)
			{
				return;
			}
			var target = new FileInfo(ctx.GetPathInTarget(file.FullName));
			if (target.Exists && Config.OnlyNewFilesNoChanges)
			{
				return;
			}
			bool restoreModel = ctx.RestoreMode;
			IoSafe(() => CopyFile(file, target, restoreModel));
		}

		const FileShare _any = FileShare.Delete | FileShare.ReadWrite;

		void CopyFile(FileInfo from, FileInfo to, bool restore)
		{
			if (to.Exists)
			{
				if (!restore && (from.Attributes & FileAttributes.Archive) == 0 && from.LastWriteTimeUtc <= to.LastWriteTimeUtc)
				{
					// both events are sensitive - archive attribute and modificatino date
					XTrace.XTrace.Verbose("CopySkip", Path.GetFileName(from.FullName));
					return;
				}
				// file existst and going to replace - reset r/o. Attribute will be restored finally
//				if (to.IsReadOnly)
//				{
//					to.IsReadOnly = false;
//				}
				var atr1 = to.Attributes;
				var atr2 = atr1 & ~(FileAttributes.Hidden | FileAttributes.ReadOnly);

				if (atr1 != atr2)
				{
					to.Attributes = atr2;
				}
			}

			XTrace.XTrace.Information("Copy", Path.GetFileName(from.FullName));

			using (var source = File.Open(from.FullName, FileMode.Open, FileAccess.Read, _any))
			{
				using (var target = File.Open(to.FullName, FileMode.Create, FileAccess.Write, FileShare.Read))
				{
					CopyStream(source, target);
					long c, a, m;
					GetFileTime(source.SafeFileHandle, out c, out a, out m);
					SetFileTime(target.SafeFileHandle, ref c, ref a, ref m);
				}
			}

			var toAttributes = from.Attributes;
			if (restore)
			{
				// restore - reset 'archive' from target, because it is in sync with backup source
				toAttributes = toAttributes & ~FileAttributes.Archive;
			}
			else
			{
				// backing up - reset 'archive' from source
				from.Attributes = from.Attributes & ~FileAttributes.Archive;
			}
			to.Attributes = toAttributes;
			// make it low level with single transaction
			//			DateTime.Now.ToFileTime
			//			to.LastAccessTimeUtc = from.LastAccessTimeUtc;
			//			to.LastWriteTimeUtc = from.LastWriteTimeUtc;
			//			to.CreationTimeUtc = from.CreationTimeUtc;
		}

		private  void CopyStream(FileStream source, FileStream target)
		{
			// source.CopyTo(target);
			int read;
			while ((read = source.Read(_copyBuffer, 0, _copyBufferSize)) != 0)
				target.Write(_copyBuffer, 0, read);
		}

		private const int _copyBufferSize = 1024 * 256;
		private readonly byte[] _copyBuffer = new byte[_copyBufferSize];

		//		static void SetFileTime(SafeFileHandle handle, DateTime create, DateTime access, DateTime modify)
		//		{
		//			var createFile = create.ToFileTimeUtc();
		//			var modifyFile = modify.ToFileTimeUtc();
		//			var accessFile = access.ToFileTimeUtc();
		//
		//			SetFileTime(handle, ref createFile, ref accessFile, ref modifyFile);
		//		}

		[DllImport("kernel32.dll")]
		static extern bool SetFileTime(SafeFileHandle hFile, ref long lpCreationTime, ref long lpLastAccessTime, ref long lpLastWriteTime);

		[DllImport("kernel32.dll")]
		static extern bool GetFileTime(SafeFileHandle hFile, out long lpCreationTime, out long lpLastAccessTime, out long lpLastWriteTime);



		static Exception IoSafe(Action act)
		{
			int max = 5;
			Exception last = null;
			for (int i = 0; i < max; i++)
			{
				try
				{
					act();
					return null;
				}
				catch (Exception ex)
				{
					last = ex;
					if (max == i)
					{
						XTrace.XTrace.Exception(ex);
					}
					Thread.Sleep(300);
				}
			}
			return last;
		}

		public void Dispose()
		{
			lock (_startingSync)
			{
				// stopping flags
				_isDisposing = true;

				var lockerSource = _lockerSource;
				if (lockerSource != null)
				{
					lockerSource.Close();
				}
				var lockerBackup = _lockerBackup;
				if (lockerBackup != null)
				{
					lockerBackup.Close();
				}
				if (IsStarted)
				{
					_watcher.EnableRaisingEvents = false;
					_watcher.Dispose();

					lock (_eventsQueue)
					{
						_eventsEnqueued.Set();
						_delayWaitEvent.Set();
					}

					// waiting
					_thread.Join();
					lock (_eventsQueue)
					{
						if (0 != _eventsQueue.Count)
						{
							XTrace.XTrace.Error("Assert: Queue should be empty on dispose: " + string.Join(", ", _eventsQueue));
						}
					}
				}

				_isDisposingBackup = true;
				BackupDirectory(SourcePath);
			}
		}
	}
}