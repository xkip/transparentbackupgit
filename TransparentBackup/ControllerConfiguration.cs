using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System;

using MyUtils;

namespace TransparentBackup
{
	public class ControllerConfiguration : Observable
	{
		bool _isEnabled;

		public bool IsEnabled
		{
			get { return _isEnabled; }
			set
			{
				_isEnabled = value;
				OnPropertyChanged("IsEnabled");
			}
		}

		bool _includeHidden;

		public bool IncludeHidden
		{
			get { return _includeHidden; }
			set
			{
				_includeHidden = value;
				OnPropertyChanged("IncludeHidden");
			}
		}

		string _from;

		public string From
		{
			get { return _from; }
			set
			{
				_from = value;
				OnPropertyChanged("From");
			}
		}

		string _to;

		public string To
		{
			get { return _to; }
			set
			{
				_to = value;
				OnPropertyChanged("To");
			}
		}

		TimeSpan? _delay;

		[DefaultValue(null)]
		public TimeSpan? Delay
		{
			get { return _delay; }
			set
			{
				_delay = value;
				OnPropertyChanged("Delay");
			}
		}

		TimeSpan? _delayForAll;

		[DefaultValue(null)]
		public TimeSpan? FullBackupEvery
		{
			get { return _delayForAll; }
			set
			{
				_delayForAll = value;
				OnPropertyChanged("Delay");
			}
		}

		private bool _onlyNewFilesNoChanges;

		[DefaultValue(false)]
		public bool OnlyNewFilesNoChanges
		{
			get { return _onlyNewFilesNoChanges; }
			set
			{
				_onlyNewFilesNoChanges = value;
				OnPropertyChanged("OnlyNewFilesNoChanges");
			}
		}
	}
}
