﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xaml;

using Microsoft.Win32;

using MyUtils.UAC;

namespace TransparentBackup
{
	public class Configuration
	{
		const string _registryPath = @"HKEY_LOCAL_MACHINE\SOFTWARE\TransparentBackup";

		public Configuration()
		{
			
		}

		static Configuration _instance;

		public static Configuration Instance
		{
			get { return _instance ?? (_instance = DeserializeFromRegistry()); }
			set
			{
				_instance = value;
				SerializeToRegistry(value);
			}
		}

		private static Configuration DeserializeFromRegistry()
		{
			var config = GetRegistryConfiguration();
			if (config == null)
			{
				return new Configuration();
			}
			return (Configuration)XamlServices.Parse(config);
		}

		private static void SerializeToRegistry(Configuration config)
		{
			if (config == null)
			{
				throw new ArgumentNullException("config");
			}
			SetRegistryConfiguration(XamlServices.Save(config));
		}

		static void SetRegistryConfiguration(string config)
		{
			Elevation.Instance.OneTimePerProcess<SaveToRegistry>(config);
		}

		public class SaveToRegistry : IElevationCall
		{
			public string Call(string args)
			{
				Registry.SetValue(_registryPath, "config", args);
				return "OK";
			}
		}

		static string GetRegistryConfiguration()
		{
			return Registry.GetValue(_registryPath, "config", null) as string;
		}

		readonly List<ControllerConfiguration> _controllers = new List<ControllerConfiguration>();

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public IList<ControllerConfiguration> Controllers { get { return _controllers; } }
	}
}
