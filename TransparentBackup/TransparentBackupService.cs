﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using Microsoft.Win32;

using TransparentBackup.Tests;
using XTrace;

namespace TransparentBackup
{
	public partial class TransparentBackupService : ServiceBase
	{
		public TransparentBackupService()
		{
			InitializeComponent();
		}

		readonly List<BackupController> _controllers = new List<BackupController>();

		public void PerformStart()
		{
#if DEBUG && TRACE
			try
			{
				new StructuredTextFileXTraceListener("TB", RelativePath.EntryAssemblyLocation) {FilesCount = 2, SizeLimit = 1024 * 1024 * 1024};
			}
			catch
			{
			}
#endif

			foreach (var controllerConfig in Configuration.Instance.Controllers.Where(x => x.IsEnabled))
			{
				var ctrl = new BackupController(controllerConfig);
				if (controllerConfig.Delay.HasValue)
				{
					ctrl.Delay = controllerConfig.Delay.Value;
				}
				if (controllerConfig.FullBackupEvery.HasValue)
				{
					ctrl.FullBackupEvery = controllerConfig.FullBackupEvery.Value;
				}
				_controllers.Add(ctrl);
			}
			foreach (var ctrl in _controllers)
			{
				ThreadPool.QueueUserWorkItem(x => ((BackupController) x).Start(), ctrl);
			}
		}

		protected override void OnStop()
		{
			PerformStop();
		}

		protected override void OnStart(string[] args)
		{
			PerformStart();
		}

		public void PerformStop()
		{
			foreach (var ctrl in _controllers)
			{
				ctrl.Dispose();
			}
		}
	}
}
