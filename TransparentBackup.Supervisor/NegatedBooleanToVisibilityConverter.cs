using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace TransparentBackup.Supervisor
{
	public class CustomBooleanToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool)
			{
				var val = (bool)value;
				if(string.Equals("negate", parameter as string, StringComparison.InvariantCultureIgnoreCase))
				{
					val = !val;
				}
				return val ? Visibility.Visible : Visibility.Collapsed;
			}
			return value;
		}
	

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}