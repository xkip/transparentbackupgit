﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Input;

using MyUtils.UAC;

using TransparentBackup.Supervisor.Elevated;

namespace TransparentBackup.Supervisor
{
	public class MainWindowsViewModel
	{
		public MainWindowsViewModel()
		{
			try
			{
				_model = Configuration.Instance;
			}
			catch
			{
				_model = new Configuration();
			}

			StartService = new DelegateCommand(StartServiceAction, CanStartService);
			StopService = new DelegateCommand(StopServiceAction, CanStopService);
			Save = new DelegateCommand(SaveAction, CanSave);
			AddConfiguration = new DelegateCommand(AddConfigurationAction);
			_configuredControllers = new ObservableCollection<ConfiguredControllerViewModel>(_model.Controllers.Select(x => new ConfiguredControllerViewModel(x, this)));

			_configuredControllers.CollectionChanged += ConfiguredControllersCollectionChanged;

			if (!_configuredControllers.Any())
			{
				AddConfigurationAction();
			}

			_timer = new Timer(5000)
			{
				Enabled = true,
			};
			_timer.Elapsed += TimerTick;
		}

		void ConfiguredControllersCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			MarkAsDirty();
		}

		void TimerTick(object sender, ElapsedEventArgs e)
		{
			StartService.RaiseCanExecuteChanged();
			StopService.RaiseCanExecuteChanged();
		}

		bool CanSave()
		{
			return _isDirty;
		}

		bool _isDirty;

		internal void MarkAsDirty()
		{
			_isDirty = true;
			Save.RaiseCanExecuteChanged();
		}

		readonly Timer _timer;

		void AddConfigurationAction()
		{
			_configuredControllers.Add(new ConfiguredControllerViewModel(new ControllerConfiguration
			{
				IsEnabled = false,
				Delay = TimeSpan.FromSeconds(30),
				From = Path.Combine(Path.GetTempPath(), "Source"),
				To = Path.Combine(Path.GetTempPath(), "Backup"),
			}, this));
		}

		void SaveAction()
		{
			_model.Controllers.Clear();
			foreach (var viewModel in ConfiguredControllers)
			{
				_model.Controllers.Add(viewModel.Model);
			}


			Configuration.Instance = _model;
			_isDirty = false;
		}

		readonly Configuration _model;

		readonly ObservableCollection<ConfiguredControllerViewModel> _configuredControllers;

		public IEnumerable<ConfiguredControllerViewModel> ConfiguredControllers
		{
			get { return _configuredControllers; }
		}

		public DelegateCommand StartService { get; private set; }
		public DelegateCommand StopService { get; private set; }
		public DelegateCommand Save { get; private set; }
		public DelegateCommand AddConfiguration { get; private set; }

		readonly Lazy<ServiceController> _serviceController = new Lazy<ServiceController>(() => new ServiceController("tb"));

		void StartServiceAction()
		{
			Elevation.Instance.OneTimePerProcess<ServiceManagementCall>("start");
		}

		bool CanStartService()
		{
			_serviceController.Value.Refresh();
			return _serviceController.Value.Status == ServiceControllerStatus.Stopped;
		}

		void StopServiceAction()
		{
			Elevation.Instance.OneTimePerProcess<ServiceManagementCall>("stop");
		}

		bool CanStopService()
		{
			_serviceController.Value.Refresh();
			return _serviceController.Value.Status == ServiceControllerStatus.Running;
		}

	}
}
