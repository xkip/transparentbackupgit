﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using MyUtils.UAC;

namespace TransparentBackup.Supervisor.Elevated
{
	class ServiceManagementCall : IElevationCall
	{
		readonly Lazy<ServiceController> _serviceController = new Lazy<ServiceController>(() => new ServiceController("tb"));

		public string Call(string args)
		{
			switch (args)
			{
				case "start":
					_serviceController.Value.Start();
					_serviceController.Value.WaitForStatus(ServiceControllerStatus.Running);
					break;
				case "stop":
					_serviceController.Value.Stop();
					_serviceController.Value.WaitForStatus(ServiceControllerStatus.Stopped);
					break;
			}
			return null;
		}
	}
}
