using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System;

using MyUtils;

namespace TransparentBackup.Supervisor
{
	public class ConfiguredControllerViewModel : Observable
	{
		readonly MainWindowsViewModel _mainWindow;

		public ConfiguredControllerViewModel(ControllerConfiguration model, MainWindowsViewModel mainWindow)
		{
			if (model == null)
			{
				throw new ArgumentNullException("model");
			}
			if (mainWindow == null)
			{
				throw new ArgumentNullException("mainWindow");
			}
			_model = model;
			_mainWindow = mainWindow;
			_model.PropertyChanged += ModelPropertyChanged;
		}

		void ModelPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			_mainWindow.MarkAsDirty();
			OnPropertyChanged(e.PropertyName);
		}

		readonly ControllerConfiguration _model;
		public ControllerConfiguration Model
		{
			get { return _model; }
		}

		public string From
		{
			get { return _model.From; }
			set { _model.From = value; }
		}

		public string To
		{
			get { return _model.To; }
			set { _model.To = value; }
		}

		public bool IsEnabled
		{
			get { return _model.IsEnabled; }
			set { _model.IsEnabled = value; }
		}

		public bool IncludeHidden
		{
			get { return _model.IncludeHidden; }
			set { _model.IncludeHidden = value; }
		}

		public bool OnlyNewFilesNoChanges
		{
			get { return _model.OnlyNewFilesNoChanges; }
			set { _model.OnlyNewFilesNoChanges = value; }
		}

		public string Delay
		{
			get { return _model.Delay.HasValue ? _model.Delay.Value.ToSpanString() : null; }
			set
			{
				if (!string.IsNullOrWhiteSpace(value))
				{
					_model.Delay = value.ToSpan("s");
				}
				else
				{
					_model.Delay = null;
				}
			}
		}

		public string FullBackupEvery
		{
			get { return _model.FullBackupEvery.HasValue ? _model.FullBackupEvery.Value.ToSpanString() : null; }
			set
			{
				if (!string.IsNullOrWhiteSpace(value))
				{
					_model.FullBackupEvery = value.ToSpan("s");
				}
				else
				{
					_model.FullBackupEvery = null;
				}
			}
		}

	}
}