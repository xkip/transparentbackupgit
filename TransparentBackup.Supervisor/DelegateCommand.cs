using System.Collections.Generic;
using System.Linq;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

using MyUtils;

namespace TransparentBackup.Supervisor
{
	public class DelegateCommand : Observable, ICommand
	{
		readonly Action<object> _action;
		readonly Func<object, bool> _canExecute;

		public DelegateCommand(Action action, Func<bool> canExecute = null)
			: this(_ => action(), canExecute == null ? default(Func<object, bool>) : _ => canExecute())
		{
			
		}

		public DelegateCommand(Action<object> action, Func<object, bool> canExecute = null)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			_action = action;
			_canExecute = canExecute;

			if (_canExecute != null)
			{
				CanExecuteChanged += DelegateCommand_CanExecuteChanged;
			}
		}

		void DelegateCommand_CanExecuteChanged(object sender, EventArgs e)
		{
			OnPropertyChanged("CanExecuteState");
		}

		public bool CanExecuteState
		{
			get { return CanExecute(null); }
		}

		public void Execute(object parameter)
		{
			try
			{
				_action(parameter);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		public bool CanExecute(object parameter)
		{
			try
			{
				return _canExecute == null || _canExecute(parameter);
			}
			catch (Exception ex)
			{
#if TRACE
				XTrace.XTrace.Exception(ex);
#endif
				return false;
			}
		}

		EventHandler _canExecuteChanged;

		public void RaiseCanExecuteChanged()
		{
			var ev = _canExecuteChanged;
			if (ev != null && _canExecute != null)
			{
				var app = Application.Current;
				if (app != null)
				{
					var disp = app.Dispatcher;
					if (disp != null)
					{
						if (Dispatcher.CurrentDispatcher != Application.Current.Dispatcher)
						{
							Application.Current.Dispatcher.BeginInvoke(ev, this, EventArgs.Empty);
						}
						else
						{
							ev(this, EventArgs.Empty);
						}
					}
				}
			}
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				if (_canExecute != null)
				{
					CommandManager.RequerySuggested += value;
					_canExecuteChanged += value;
				}
			}
			remove
			{
				if (_canExecute != null)
				{
					CommandManager.RequerySuggested -= value;
					_canExecuteChanged -= value;
				}
			}
		}

		public static implicit operator DelegateCommand(Action act)
		{
			return new DelegateCommand(act);
		}
	}
}