﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using MyUtils;

namespace TransparentBackup.Tests
{
	[TestClass]
	public class UnitTest1
	{
		[ClassInitialize]
		public static void ClassInitialize(TestContext c)
		{
#if TRACE
			new XTrace.StructuredTextFileXTraceListener("Backup Tests", XTrace.RelativePath.CurrentDir);
#endif
		}

		[ClassCleanup]
		public static void ClassCleanup()
		{

		}

		[TestInitialize]
		public void Init()
		{
			if (Directory.Exists("ramdisk"))
			{
				Directory.Delete("ramdisk", true);
			}
			if (Directory.Exists("backup"))
			{
				Directory.Delete("backup", true);
			}
			Directory.CreateDirectory("ramdisk");
			Directory.CreateDirectory("backup");
		}

		TimeSpan _delay = TimeSpan.FromSeconds(1);
		Action<BackupController> _controllerFiller = delegate { };

		Action<BackupController> InsideController
		{
			set
			{
				using (var ctrl = new BackupController(SourcePath, BackupPath)
				{
					Delay = _delay,
				})
				{
					_controllerFiller(ctrl);
					ctrl.Start();
					ctrl.PrimarySyncDoneWaitHandle.WaitOne();
					value(ctrl);
				}
			}
		}

		static string SourcePath
		{
			get { return Path.GetFullPath("ramdisk"); }
		}

		static string BackupPath
		{
			get { return Path.GetFullPath("backup"); }
		}

		static string FileRead(string fileName)
		{
			using (var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Delete | FileShare.ReadWrite))
			{
				using (var sr = new StreamReader(file))
				{
					return sr.ReadToEnd();
				}
			}
		}

		[TestMethod]
		public void Should_40_backup_with_reasonable_delay()
		{
			// Setup
			File.WriteAllText("ramdisk/filex", "");
			InsideController = delegate
			{
			};

#if TRACE
			XTrace.XTrace.Important("File Created");
#endif			// Execute
            InsideController = delegate
			{
				for (int i = 0; i < 4; i++)
				{
					var body = i + "";
					for (int j = 0; j < 5; j++)
					{
						File.WriteAllText("ramdisk/filex", body + j);
					}
					File.WriteAllText("ramdisk/filex", body);

					Thread.Sleep(500);
					Assert.AreNotEqual(body, FileRead("backup/filex"));
					// todo waithandle
					Thread.Sleep(1600);
					Assert.AreEqual(body, FileRead("backup/filex"));

				}
			};
		}

		[TestMethod]
		public void Should_40_not_backup_or_restore_when_controller_id_missmatched()
		{
			// Setup
			InsideController = delegate
			{
				File.WriteAllText("ramdisk/oldFile", "ver1");
			};
			KillRamDisk();

			Assert.IsFalse(File.Exists("ramdisk/oldFile"));
			Assert.IsTrue(File.Exists("backup/oldFile"));
			// mark as different containers
			File.WriteAllText("ramdisk/_backupControlelrId.TransparentBackup", Guid.NewGuid().ToString());

			// Execute
			InsideController = delegate
			{
				File.WriteAllText("ramdisk/newFile", "ver1");
			};
			// Verify
			Assert.IsTrue(File.Exists("ramdisk/newFile"));
			Assert.IsFalse(File.Exists("ramdisk/oldFile"));
			Assert.IsFalse(File.Exists("backup/newFile"));
			Assert.IsTrue(File.Exists("backup/oldFile"));
		}

		[TestMethod]
		public void Should_30_backup_incrementally()
		{
			// Setup
			File.WriteAllBytes("ramdisk/largefile", new byte[100 * 1024 * 1024]);
			// Execute
			InsideController = delegate
			{
				for (int i = 0; i < 10; i++)
				{
					File.WriteAllText("ramdisk/file", "ver" + i);
					Thread.Sleep(1100);
					// todo waithandle
					Assert.AreEqual("ver" + i, FileRead("backup/file"));
					// Should not copy 100 MB file every time! Just one changed file!
				}
			};
		}

		[TestMethod]
		public void Should_60_convert_10_changes_per_second_into_single()
		{
			// Setup
			_delay = TimeSpan.FromSeconds(8);

			int changes = 0;
			using (var fsw = new FileSystemWatcher(BackupPath, "*.*")
			{
				EnableRaisingEvents=true,
				IncludeSubdirectories = true,
				NotifyFilter=NotifyFilters.LastWrite|NotifyFilters.CreationTime,
			})
			{
				fsw.Changed += (o, e) =>
				{
					changes++;
					Console.WriteLine(e.Name + " " + e.ChangeType);
				};

				// Execute
				InsideController = delegate
				{
					Thread.Sleep(10000); // wait for timers sync and for first backup
					changes = 0; // reset changing in special files
					Console.WriteLine("reset");
					for (int i = 0; i < 10; i++)
					{
						File.WriteAllText("ramdisk/file", "ver" + i);
					}
					Thread.Sleep(2500);
					Assert.AreEqual(0, changes);

					for (int i = 0; i < 10; i++)
					{
						File.WriteAllText("ramdisk/file", "ver" + i);
					}
					Thread.Sleep(2500);
					Assert.AreEqual(0, changes);

					Thread.Sleep(4000);
					Console.WriteLine("big phase");
					Assert.AreEqual(1, changes);
				};

			}

			Assert.AreEqual(1, changes);

		}

		[TestMethod]
		public void Should_10_backup_immediately_on_dispose()
		{
			// Setup
			// Execute
			InsideController = delegate
			{
				File.WriteAllText("ramdisk/test", "ver");
			};
			// Verify
			Assert.AreEqual("ver", File.ReadAllText("backup/test"));
		}

		[TestMethod]
		public void Should_50_have_full_access_to_files_all_the_time()
		{
			// Setup
			// Execute
			InsideController = delegate
			{
				for (int i = 0; i < 1000; i++)
				{
					File.WriteAllText("ramdisk/test", "ver" + i);
					Thread.Sleep(1);
				}
			};
			// Verify
			Assert.AreEqual("ver999", File.ReadAllText("backup/test"));
		}

		[TestMethod]
		public void Should_05_prepare_path_with_slashes()
		{
			var sut = new BackupController();
			sut.BackupPath = "C:\\asd/asd\\asd/q";
			Assert.AreEqual("C:\\asd\\asd\\asd\\q", sut.BackupPath);
			sut.SourcePath = "C:\\asd/asd\\asd/q";
			Assert.AreEqual("C:\\asd\\asd\\asd\\q", sut.SourcePath);
		}

		[TestMethod]
		public void Should_05_trim_spaces_at_the_end()
		{
			var sut = new BackupController();
			sut.BackupPath = "C:\\asd\\";
			Assert.AreEqual("C:\\asd", sut.BackupPath);
			sut.SourcePath = "C:\\asd\\";
			Assert.AreEqual("C:\\asd", sut.SourcePath);
		}

		[TestMethod]
		public void Should_20_restore_files_on_empty_source()
		{
			// setup
			File.WriteAllText("ramdisk/test1.txt", "ver1");

			// start controlelr
			InsideController = delegate
			{
				// some changes
				File.WriteAllText("ramdisk/test1.txt", "ver2");
				File.WriteAllText("ramdisk/test2.txt", "ver2");

				// shutdown
			};

			// kill source
			KillRamDisk();


			// start controlelr
			InsideController = delegate
			{
				Utils.WaitForEx(() => File.Exists("ramdisk/test2.txt"), 1000);
			};

			// everything is restored
			Assert.AreEqual("ver2", File.ReadAllText("ramdisk/test1.txt"));
			Assert.AreEqual("ver2", File.ReadAllText("ramdisk/test2.txt"));


			// kill source
			KillRamDisk();


			// start controlelr
			InsideController = delegate
			{
				Utils.WaitForEx(() => File.Exists("ramdisk/test2.txt"), 1000);
			};

			// everything is restored
			Assert.AreEqual("ver2", File.ReadAllText("ramdisk/test1.txt"));
			Assert.AreEqual("ver2", File.ReadAllText("ramdisk/test2.txt"));

		}

		[TestMethod]
		public void Should_15_backup_files()
		{
			// Setup
			File.WriteAllText("ramdisk/test1.txt", "ver1");

			// Execute
			// start controlelr
			InsideController = delegate
			{
			};
			// Verify
			Assert.AreEqual("ver1", File.ReadAllText("backup/test1.txt"));
		}

		[TestMethod]
		public void Should_30_handle_offline_changes()
		{
			// Setup
			File.WriteAllText("ramdisk/1","1");
			InsideController = delegate { };
			File.WriteAllText("ramdisk/2", "2");
			// Execute
			InsideController = delegate { };
			// Verify
			File.Exists("backup/1");
			File.Exists("backup/2");
		}

		[TestMethod]
		public void Should_40_hidden_files_should_be_excluded()
		{
			// Setup
			File.WriteAllText("ramdisk/1", "1");
			var fi = new FileInfo("ramdisk/1");
			fi.Attributes = fi.Attributes | FileAttributes.Hidden;

			// Execute
			InsideController = delegate { };

			// Verify
			Assert.IsFalse(File.Exists("backup/1"));
			KillRamDisk();
			InsideController = delegate { };
			Assert.IsFalse(File.Exists("ramdisk/1"));
			Assert.IsFalse(File.Exists("backup/1"));

			// fi.Refresh();
			// Assert.IsTrue((fi.Attributes | FileAttributes.Hidden) > 0);

		}

		[TestMethod]
		public void Should_50_backup_and_restore_readonly_files()
		{
			// Setup
			File.WriteAllText("ramdisk/ro", "1");
			File.WriteAllText("ramdisk/no", "1");
			var fi1Source = new FileInfo("ramdisk/ro");
			var fi1Backup = new FileInfo("backup/ro");
			var fi2Source = new FileInfo("ramdisk/no");
			var fi2Backup = new FileInfo("backup/no");
			fi1Source.IsReadOnly = true;
			fi2Source.IsReadOnly = false;
			// Execute

			InsideController = delegate { };

			// Verify
			Assert.IsTrue(fi1Backup.IsReadOnly);
			Assert.IsFalse(fi2Backup.IsReadOnly);

			fi1Source.IsReadOnly = false;
			KillRamDisk();

			InsideController = delegate { };

			// Verify
			fi1Backup.Refresh();
			fi2Backup.Refresh();
			fi1Source.Refresh();
			fi2Source.Refresh();

			Assert.IsTrue(fi1Backup.IsReadOnly);
			Assert.IsFalse(fi2Backup.IsReadOnly);
			Assert.IsTrue(fi1Source.IsReadOnly);
			Assert.IsFalse(fi2Source.IsReadOnly);

			fi1Source.IsReadOnly = false;
			fi1Backup.IsReadOnly = false;
		}

		static void KillRamDisk()
		{
			Directory.Delete("ramdisk", true);
			Directory.CreateDirectory("ramdisk");
		}

		[TestMethod]
		public void Should_55_be_able_to_restore_several_times()
		{
			// Setup
			File.WriteAllText("ramdisk/data", "1");
			InsideController = delegate { };
			// Execute
			for (int i = 0; i < 5; i++)
			{
				KillRamDisk();
				InsideController = delegate { };
				File.Exists("ramdisk/data");
				Assert.AreEqual("1", File.ReadAllText("ramdisk/data"));
			}

			Should_30_backup_incrementally();
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void Should_08_not_allow_call_start_several_times()
		{
			// Setup
			Directory.CreateDirectory("a");
			Directory.CreateDirectory("b");
			using (var ctrl = new BackupController(Path.GetFullPath("a"), Path.GetFullPath("b")))
			{
				// Execute
				ctrl.Start();
				// Verify
				ctrl.Start();
			}
		}

		[TestMethod]
		public void Should_55_hidden_directories_should_not_be_backed_up()
		{
			// Setup
			Directory.CreateDirectory("ramdisk/dir");
			File.WriteAllText("ramdisk/dir/1", "1");
			var di = new DirectoryInfo("ramdisk/dir");
			di.Attributes |= FileAttributes.Hidden;

			// Execute
			InsideController = delegate { };
			KillRamDisk();
			InsideController = delegate { };

			// Verify
			di.Refresh();
			Assert.IsFalse(di.Exists);
			//Assert.IsTrue((di.Attributes & FileAttributes.Hidden) > 0);
		}

		[TestMethod]
		public void Should_35_subdirs_of_nth_level()
		{
			// Setup
			Directory.CreateDirectory("ramdisk/1/11/111");
			File.WriteAllText("ramdisk/1/11/111/file", "ver1");
			// Execute
			InsideController = delegate
			{
				Assert.AreEqual("ver1", FileRead("backup/1/11/111/file"));
				File.WriteAllText("ramdisk/1/11/111/file", "ver2");
				Thread.Sleep(2100);
				Assert.AreEqual("ver2", FileRead("backup/1/11/111/file"));

				File.Move("ramdisk/1/11/111/file", "ramdisk/1/11/111/file2");
				Thread.Sleep(2100);
				Assert.AreEqual("ver2", FileRead("backup/1/11/111/file2"));
			};
		}

		[TestMethod]
		public void Should_45_attributes_of_source_file_is_not_changed()
		{
			// Setup
			var fi = new FileInfo("ramdisk/test");
			File.WriteAllText(fi.FullName, "1");
			fi.Refresh();
			fi.Attributes |= FileAttributes.System | FileAttributes.Temporary;
			// Execute
			InsideController = delegate { };
			// Verify
			KillRamDisk();
			InsideController = delegate { };
			fi.Refresh();
			Assert.IsTrue((fi.Attributes & FileAttributes.System) > 0);
			Assert.IsTrue((fi.Attributes & FileAttributes.Temporary) > 0);

			KillRamDisk();
			InsideController = delegate { };
			fi.Refresh();
			Assert.IsTrue((fi.Attributes & FileAttributes.System) > 0);
			Assert.IsTrue((fi.Attributes & FileAttributes.Temporary) > 0);
		}

		[TestMethod]
		[ExpectedException(typeof(InvalidOperationException))]
		public void Should_not_allow_have_two_controllers_on_the_same_folders()
		{
			Assert.Inconclusive();
			InsideController = delegate
			{
				using (var sut = new BackupController()
				{
					SourcePath = SourcePath,
					BackupPath = BackupPath,
				})
				{
					sut.Start();
				}
			};
		}

		[TestMethod]
		public void Should_60_delete_from_backup_on_deletion()
		{
			// Setup
			File.WriteAllText("ramdisk/1", "1");
			InsideController = delegate
			{
				Thread.Sleep(2100);
				File.Delete("ramdisk/1");
				Thread.Sleep(2100);
				Assert.IsFalse(File.Exists("backup/1"));
			};
		}

		[TestMethod]
		public void Should_60_delete_from_backup_on_deletion_offline()
		{
			// Setup
			File.WriteAllText("ramdisk/1", "1");
			InsideController = delegate { };
			File.Delete("ramdisk/1");
			InsideController = delegate { };
			Assert.IsFalse(File.Exists("backup/1"));
		}

		[TestMethod]
		public void Should_deletefrom_backup_on_rename()
		{
			// Setup
			File.WriteAllText("ramdisk/a","a");
			// Execute
			InsideController = delegate
			{
				File.Move("ramdisk/a", "ramdisk/b");
				Thread.Sleep(2100);
				Assert.IsFalse(File.Exists("ramdisk/a"));
				Assert.IsFalse(File.Exists("backup/a"));
				Assert.IsTrue(File.Exists("ramdisk/b"));
				Assert.IsTrue(File.Exists("backup/b"));
			};

		}

		[TestMethod]
		public void Should_delete_from_backup_on_folder_move()
		{
			// Setup
			Directory.CreateDirectory("ramdisk/a/peter");
			File.WriteAllText("ramdisk/a/peter/x", "ver");
			// Execute
			InsideController = delegate
			{
				Directory.CreateDirectory("ramdisk/b");
				Directory.Move("ramdisk/a/peter", "ramdisk/b/peter");
				Thread.Sleep(2100);
				Assert.IsFalse(Directory.Exists("ramdisk/a/peter"));
				Assert.IsTrue(Directory.Exists("ramdisk/a"));
				Assert.IsTrue(Directory.Exists("ramdisk/b/peter"));
				Assert.AreEqual("ver", FileRead("ramdisk/b/peter/x"));
				Assert.AreEqual("ver", FileRead("backup/b/peter/x"));
			};
			// Verify

			Assert.IsFalse(Directory.Exists("ramdisk/a/peter"));
			Assert.IsTrue(Directory.Exists("ramdisk/a"));
			Assert.IsTrue(Directory.Exists("ramdisk/b/peter"));
			Assert.AreEqual("ver", FileRead("ramdisk/b/peter/x"));
			Assert.AreEqual("ver", FileRead("backup/b/peter/x"));

			KillRamDisk();
			InsideController = delegate { };

			Assert.IsFalse(Directory.Exists("ramdisk/a/peter"));
			Assert.IsTrue(Directory.Exists("ramdisk/a"));
			Assert.IsTrue(Directory.Exists("ramdisk/b/peter"));
			Assert.AreEqual("ver", FileRead("ramdisk/b/peter/x"));
			Assert.AreEqual("ver", FileRead("backup/b/peter/x"));
		}

		[TestMethod]
		public void Should_delete_from_backup_on_folder_move_easy()
		{
			// Setup
			Directory.CreateDirectory("ramdisk/a");
			File.WriteAllText("ramdisk/a/x", "ver");
			// Execute
			InsideController = delegate
			{
				Directory.Move("ramdisk/a", "ramdisk/b");
				Thread.Sleep(2100);
				Assert.IsFalse(Directory.Exists("ramdisk/a"));
				Assert.IsFalse(Directory.Exists("backup/a"));
				Assert.IsTrue(Directory.Exists("ramdisk/b"));
				Assert.IsTrue(Directory.Exists("backup/b"));
				Assert.AreEqual("ver", FileRead("ramdisk/b/x"));
				Assert.AreEqual("ver", FileRead("backup/b/x"));
			};
			// Verify

			Assert.IsFalse(Directory.Exists("ramdisk/a"));
			Assert.IsFalse(Directory.Exists("backup/a"));
			Assert.IsTrue(Directory.Exists("ramdisk/b"));
			Assert.IsTrue(Directory.Exists("backup/b"));
			Assert.AreEqual("ver", FileRead("ramdisk/b/x"));
			Assert.AreEqual("ver", FileRead("backup/b/x"));

			KillRamDisk();
			InsideController = delegate { };

			Assert.IsFalse(Directory.Exists("ramdisk/a"));
			Assert.IsFalse(Directory.Exists("backup/a"));
			Assert.IsTrue(Directory.Exists("ramdisk/b"));
			Assert.IsTrue(Directory.Exists("backup/b"));
			Assert.AreEqual("ver", FileRead("ramdisk/b/x"));
			Assert.AreEqual("ver", FileRead("backup/b/x"));
		}

		[TestMethod]
		public void Should_detect_forced_unmount()
		{
			// Setup
			// Execute
			// Verify
			
			Assert.Inconclusive();
		}


		[TestMethod]
		public void Should_backup_and_restore_hidden_files()
		{
			_controllerFiller = x => x.Config.IncludeHidden = true;

			InsideController = delegate
			{
				File.WriteAllText("ramdisk/1.suo", "1");
				Directory.CreateDirectory("ramdisk/hid");

				File.SetAttributes("ramdisk/1.suo", FileAttributes.Hidden | FileAttributes.Normal);
				File.SetAttributes("ramdisk/hid", FileAttributes.Hidden | FileAttributes.Normal);
			};

			// kill source
			KillRamDisk();

			// start controlelr
			InsideController = delegate
			{
				Utils.WaitForEx(() => File.Exists("ramdisk/1.suo"), 1000);
				Utils.WaitForEx(() => Directory.Exists("ramdisk/hid"), 1000);
			};
		}

		[TestMethod]
		public void Should_not_backup_and_restore_system_files()
		{
			InsideController = delegate
			{
				File.WriteAllText("ramdisk/1.suo", "1");
				Directory.CreateDirectory("ramdisk/hid");

				File.SetAttributes("ramdisk/1.suo", FileAttributes.System | FileAttributes.Normal);
				File.SetAttributes("ramdisk/hid", FileAttributes.System | FileAttributes.Normal);
			};

			// kill source
			KillRamDisk();

			// start controlelr
			InsideController = delegate
			{
			};

			Assert.IsFalse(File.Exists("ramdisk/1.suo"));
			Assert.IsFalse(Directory.Exists("ramdisk/hid"));
		}

		[TestMethod]
		public void Should_create_destination()
		{
			Directory.Delete("backup");
			InsideController = delegate { };
			Assert.IsTrue(Directory.Exists("backup"));
		}

		[TestMethod]
		public void Should_restore_even_with_bad_folders_at_source()
		{
			Directory.CreateDirectory("ramdisk/1");
			Directory.CreateDirectory("ramdisk/2");
			InsideController = delegate { };
			KillRamDisk();
			Directory.CreateDirectory("ramdisk/2");
			Directory.CreateDirectory("ramdisk/3");

			InsideController = delegate { };

			Assert.IsTrue(Directory.Exists("ramdisk/1"));
			Assert.IsTrue(Directory.Exists("ramdisk/2"));
			Assert.IsFalse(Directory.Exists("ramdisk/3"));
		}

		[TestMethod]
		public void Should_exclude_tmp()
		{
			Directory.CreateDirectory("ramdisk/tmp");
			var tmp = Path.GetFullPath("ramdisk/tmp");
			Environment.SetEnvironmentVariable("tmp", tmp);
			Environment.SetEnvironmentVariable("temp", tmp);

			InsideController = delegate
			{
				File.WriteAllText("ramdisk/1", "");
				File.WriteAllText("ramdisk/tmp/1", "");
			};

			KillRamDisk();

			InsideController = delegate { };

			Assert.IsTrue(File.Exists("ramdisk/1"));
			Assert.IsFalse(File.Exists("ramdisk/tmp/1"));
		}

		[TestMethod]
		public void Should_preserve_datetimes()
		{
			File.WriteAllText("ramdisk/1", "");
			File.SetCreationTimeUtc("ramdisk/1", new DateTime(2000, 1, 1));
			File.SetLastAccessTimeUtc("ramdisk/1", new DateTime(2000, 1, 2));
			File.SetLastWriteTimeUtc("ramdisk/1", new DateTime(2000, 1, 3));

			InsideController = delegate { };
			KillRamDisk();
			InsideController = delegate { };

			Assert.AreEqual(new DateTime(2000, 1, 3), File.GetLastWriteTimeUtc("ramdisk/1"));
			Assert.AreEqual(new DateTime(2000, 1, 1), File.GetCreationTimeUtc("ramdisk/1"));
			Assert.AreEqual(new DateTime(2000, 1, 2), File.GetLastAccessTimeUtc("ramdisk/1"));
		}
	}
}
