﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MyUtils.UAC.Internal;

namespace MyUtils.UAC.TestConsole
{
	class Program
	{
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(Elevation.IsElevated);

                var myCom = ElevationFactory.LaunchElevatedComObjectApi();
                Console.WriteLine(myCom.TestAdmin());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                ElevationFactory.ReleaseAllComObject();
            }
        }
	}
}
