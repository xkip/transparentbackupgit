﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using TransparentBackup.Tests;

namespace TransparentBackup.Service
{
	public partial class TransparentBackupService : ServiceBase
	{
		public TransparentBackupService()
		{
			InitializeComponent();
		}

		List<BackupController> _controllers = new List<BackupController>();

		protected override void OnStart(string[] args)
		{
		}

		protected override void OnStop()
		{
		}
	}
}
