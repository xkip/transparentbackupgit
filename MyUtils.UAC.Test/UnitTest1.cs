﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;

namespace MyUtils.UAC.Test
{
	/// <summary>
	/// Should request elevation only once per test session
	/// </summary>
	[TestClass]
	public class UnitTest1
	{
		[ClassInitialize]
		public static void Tests(TestContext x)
		{
			Assert.AreEqual(false, Elevation.Instance.IsElevatedOneTimePerProcess);
			Assert.AreEqual(false, Elevation.Instance.IsElevated);
			Assert.AreEqual(false, Elevation.Instance.IsElevatedOrOneTimePerProcess);
		}

		[TestMethod]
		public void Should_parse_strings()
		{
			CollectionAssert.AreEqual(new[] {"ab", "cd", "ef"}, "ab*cd*ef".Parse(3, '*'));
			CollectionAssert.AreEqual(new[] {"ab", "cd"}, "ab*cd".Parse(2, '*'));
			CollectionAssert.AreEqual(new[] { "ab" }, "ab".Parse(1, '*'));
			CollectionAssert.AreEqual(new[] {"ab*cd"}, "ab*cd".Parse(1, '*'));

			CollectionAssert.AreEqual(new[] {"ab", "cd*ef"}, "ab*cd*ef".Parse(2, '*'));
			//CollectionAssert.AreEqual(new string[] {  }, "123*123".Parse(0, '*'));

		}

		[TestMethod]
		public void Should_be_abel_to_change_hklm_and_request_only_once()
		{
			PerformTest();
			PerformTest();
			PerformTest();
		}

		[TestMethod]
		public void Should_have_singletone_instance()
		{
			var hash1 = Elevation.Instance.OneTimePerProcess<MyTestCallHashCode>();
			var hash2 = Elevation.Instance.OneTimePerProcess<MyTestCallHashCode>();
			var hash3 = Elevation.Instance.OneTimePerProcess<MyTestCallHashCode>();

			Console.WriteLine(hash1);
			int i;
			Assert.IsTrue(int.TryParse(hash1, out i));
			Assert.AreEqual(hash1, hash2);
			Assert.AreEqual(hash1, hash3);
		}

		[TestMethod]
		public void Should_rethrow_exception_but_not_crushing_elevated_process()
		{
			var hash1 = Elevation.Instance.OneTimePerProcess<MyTestCallHashCode>();
			AssertThrows(() => Elevation.Instance.OneTimePerProcess<MyTestCallException>());
			AssertThrows(() => Elevation.Instance.OneTimePerProcess<MyTestCallException>());
			AssertThrows(() => Elevation.Instance.OneTimePerProcess<MyTestCallException>());
			var hash2 = Elevation.Instance.OneTimePerProcess<MyTestCallHashCode>();

			Assert.AreEqual(hash1, hash2);
		}

		Exception Record(Action act)
		{
			try
			{
				act();
				return null;
			}
			catch(Exception ex)
			{
				return ex;
			}
		}

		void AssertThrows(Action act)
		{
			var ex = Record(act);
			Assert.IsNotNull(ex);
			Assert.IsInstanceOfType(ex, typeof(InvalidOperationException));
			StringAssert.Contains(ex.Message, "testmsg");
		}

		static void PerformTest()
		{
			var rnd = new Random().Next();
			var res = Elevation.Instance.OneTimePerProcess<MyTestCall>(rnd.ToString());
			Assert.AreEqual("Done " + rnd, res);

			var reg = Registry.GetValue("HKEY_LOCAL_MACHINE", "test", "");
			Assert.AreEqual(rnd.ToString(), reg);
		}
	}

	public class MyTestCallHashCode : IElevationCall
	{
		public string Call(string args)
		{
			return GetHashCode().ToString();
		}
	}

	public class MyTestCallException : IElevationCall
	{
		public string Call(string args)
		{
			throw new InvalidOperationException("testmsg");
		}
	}

	public class MyTestCall : IElevationCall
	{
		public string Call(string args)
		{
			try
			{
				Registry.SetValue("HKEY_LOCAL_MACHINE", "test", args);
				return "Done " + args;
			}
			catch(Exception ex)
			{
				return ex.ToString();
			}
		}
	}
}
